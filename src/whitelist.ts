export function whitelist (list, config = {}) {
  function origin (orig, callback) {
    if (orig === undefined || list.indexOf(orig) >= 0) {
      callback(null, true)
    } else {
      callback(new Error(`not allowed by cors: ${orig}`))
    }
  }

  return {
    ...config,
    origin
  }
}
