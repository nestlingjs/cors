# Cors helpers for NestJS

Helpers:
 - whitelist(list, corsConfig = {})

## Setup

```typescript
import { NestFactory } from '@nestjs/core'
import { whitelist } from '@nestling/cors'
import { ApplicationModule } from './app/app.module'
import { INestApplication } from '@nestjs/common/interfaces/nest-application.interface'
import { environment } from './environments/environment'

const app: Promise<INestApplication> = NestFactory.create(ApplicationModule)

app.then(instance => {
  instance.enableCors(whitelist(environment.whitelist, {}))
  ...
})
```
